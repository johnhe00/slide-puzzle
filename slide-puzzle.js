var PUZZLE_NUM_ROWS = 3;
var PUZZLE_NUM_COLS = 3;

var canvas;
var ctx;
var mouse;

var img;
var pieces;
var pieceWidth;
var pieceHeight;
var puzzleWidth;
var puzzleHeight;
var emptyPiece;
var valid;

window.onload = init;

function init() {
	img = new Image();
	img.src = "t-rex.jpg";
	pieceWidth = Math.floor(img.width / PUZZLE_NUM_COLS);
	pieceHeight = Math.floor(img.height / PUZZLE_NUM_ROWS);
	puzzleWidth = pieceWidth * PUZZLE_NUM_COLS;
	puzzleHeight = pieceHeight * PUZZLE_NUM_ROWS;
	setCanvas();
	initPuzzle();
}

function setCanvas() {
	canvas = document.getElementById('canvas');
	ctx = canvas.getContext('2d');
	canvas.width = puzzleWidth;
	canvas.height = puzzleHeight;
}

function initPuzzle() {
	pieces = [];
	mouse = {x:0, y:0};
	valid = false;
	document.getElementById('counter').innerHTML = 0;
	buildPieces();
	draw();
	canvas.onmouseup = updatePuzzle;
}

function buildPieces() {
	var piece;
	var xPos = 0;
	var yPos = 0;
	emptyPiece = 0;
	for(var i = 0; i < PUZZLE_NUM_ROWS * PUZZLE_NUM_COLS; i++) {
		piece = {};
		if(i != emptyPiece) {
			piece.x = xPos;
			piece.y = yPos;
		}
		else {
			piece.x = -500;
			piece.y = -500;
		}
		pieces.push(piece);
		xPos += pieceWidth;
		if(xPos >= puzzleWidth) {
			xPos = 0;
			yPos += pieceHeight;
		}
	}
}

function draw() {
	ctx.clearRect(0, 0, puzzleWidth, puzzleHeight);
	var xPos = 0;
	var yPos = 0;
	for(var i = 0; i < pieces.length; i++){
		var piece = pieces[i];
		if (piece.x >= 0) {
			ctx.drawImage(img, piece.x, piece.y, pieceWidth, pieceHeight, xPos, yPos, pieceWidth, pieceHeight);
		}
		else {
			ctx.fillStyle = "#aaaaaa";
			ctx.fillRect(xPos, yPos, pieceWidth, pieceHeight);
		}
		ctx.strokeRect(xPos, yPos, pieceWidth, pieceHeight);
		xPos += pieceWidth;
		if(xPos >= puzzleWidth) {
			xPos = 0;
			yPos += pieceHeight;
		}
	}
}

function updatePuzzle (e) {
	if(e.button != 0) {
		return;
	}
	getMousePos(e);
	
	var clickedPiece = checkPieceClicked();
	
	if(clickedPiece != null) {
		var positions = getNeighboursIndex(clickedPiece);
		var index = positions.indexOf(emptyPiece);
		if(index >= 0) {
			swapPiece(clickedPiece);
			moveCount();
			draw();
			if(checkWin() && valid) {
				alert("You Win! It took you " + document.getElementById('counter').innerHTML + " moves!");
				valid = false;
				document.getElementById('counter').innerHTML = 0;
			}
		}
	}
}

function getMousePos(e) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	mouse.x = e.clientX - rect.left - root.scrollLeft;
	mouse.y = e.clientY - rect.top - root.scrollTop;

	var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	if(!is_chrome) {
		mouse.x += window.pageXOffset;
		mouse.y += window.pageYOffset;
	}
}

function moveCount() {
	if(valid) {
		document.getElementById('counter').innerHTML++;
	}
}

function checkPieceClicked() {
	var xPos = 0;
	var yPos = 0;
	
	for(var i = 0; i < pieces.length; i++) {
		if(mouse.x < xPos || mouse.x > (xPos + pieceWidth) || mouse.y < yPos || mouse.y > (yPos + pieceHeight)){
			//PIECE NOT HIT
		}
		else{
			return i;
		}
		xPos += pieceWidth;
		if(xPos >= puzzleWidth) {
			xPos = 0;
			yPos += pieceHeight;
		}
	}
	return null;
}

function checkWin() {
	var gameWon = true;
	var xPos = 0;
	var yPos = 0;
	for(var i = 0; i < pieces.length; i++) {
		var piece = pieces[i];
		if((piece.x != xPos || piece.y != yPos) && i != 0){
			return false;
		}
		xPos += pieceWidth;
		if(xPos >= puzzleWidth) {
			xPos = 0;
			yPos += pieceHeight;
		}
	}
	
	return true;
}

function shufflePuzzle() {
	valid = true;
	for(var i = 0; i < pieces.length; i++) {
		if(pieces[i].x < 0) {
			emptyPiece = i;
			break;
		}
	}
	for (var i = 0; i < 128; i++) {
		var positions = getNeighboursIndex(emptyPiece);
		var randomPos = Math.floor(Math.random() * positions.length);
		swapPiece(positions[randomPos]);
	}
	
	draw();
}

function swapPiece(index) {
	var piece = pieces[emptyPiece];
	pieces[emptyPiece] = pieces[index];
	pieces[index] = piece;
	emptyPiece = index;
}

function getNeighboursIndex(num) {
	var positions = [];
	
	var newPos = num - 1;
	if(num % PUZZLE_NUM_COLS != 0 && newPos >= 0) {
		positions.push(newPos);
	}
	newPos = num + 1;
	if(num % PUZZLE_NUM_COLS != PUZZLE_NUM_COLS - 1 && newPos < pieces.length ) {
		positions.push(newPos);
	}
	newPos = num - PUZZLE_NUM_COLS;
	if(newPos >= 0) {
		positions.push(newPos);
	}
	newPos = num + PUZZLE_NUM_COLS;
	if(newPos < pieces.length ) {
		positions.push(newPos);
	}
	return positions;
}

function setEasy() {
	PUZZLE_NUM_ROWS = 3;
	PUZZLE_NUM_COLS = 3;
	init();
}

function setHard() {
	PUZZLE_NUM_ROWS = 4;
	PUZZLE_NUM_COLS = 4;
	init();
}

function setTRex() {
	PUZZLE_NUM_ROWS = 5;
	PUZZLE_NUM_COLS = 5;
	init();
}
